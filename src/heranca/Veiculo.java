package heranca;

public class Veiculo {
	//modelo, placa, fabricante, comprimento, tipo e id
	//modelo, placa, fabricante, ano , cor e id.
	protected  int ano;
	protected String modelo;
	protected int placa;
	protected String fabricante;
	protected int id;
	public int getAno() {
		return ano;
	}
	public void setAno(int ano) {
		this.ano = ano;
	}
	public String getModelo() {
		return modelo;
	}
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	public int getPlaca() {
		return placa;
	}
	public void setPlaca(int placa) {
		this.placa = placa;
	}
	public String getFabricante() {
		return fabricante;
	}
	public void setFabricante(String fabricante) {
		this.fabricante = fabricante;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	
}