package heranca;

public class Carreta extends Veiculo {
	
	//modelo, placa, fabricante, ano , cor e id.
	
	public Tipo tipo;
	public int comprimento;
	
	enum Tipo {
		Grao,
		Liquido,
		Bau;
	}

	public Tipo getTipo() {
		return tipo;
	}

	public void setTipo(Tipo tipo) {
		this.tipo = tipo;
	}

	public int getComprimento() {
		return comprimento;
	}

	public void setComprimento(int comprimento) {
		this.comprimento = comprimento;
	}

}
