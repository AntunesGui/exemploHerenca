package heranca;

public class App {
	public static void main(String[] args) {
		Carreta carreta = new Carreta();
		carreta.setId(1);
		carreta.setAno(1991);
		carreta.setModelo("Ford GTX");
		carreta.setPlaca(0104);
		carreta.setTipo(Carreta.Tipo.Grao);
		carreta.setComprimento(100);

		Carro carro = new Carro();
		carro.setId(2);
		carro.setAno(2018);
		carro.setModelo("Mustang GTX");
		carro.setPlaca(0111);
		carro.setCor("Branco");

		Caminhao caminhao = new Caminhao();
		caminhao.setId(1);
		caminhao.setAno(1991);
		caminhao.setModelo("Ford GTX");
		caminhao.setPlaca(0104);
		caminhao.setCor("Vinho");


		caminhao.listaCarretas.add(carreta);

		for(Carreta carreta_ : caminhao.listaCarretas) {
			System.out.println(carreta_.ano); 
			System.out.println(carreta_.comprimento);
		}

	}
}
